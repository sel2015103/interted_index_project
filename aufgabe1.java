import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.nio.file.*;


public class aufgabe1 {
       public static void main(String[] args) throws IOException {

          System.out.println("loading "+args[0]+" file");
          long start = System.currentTimeMillis();
          //버퍼사이즌 파일크기에 영향을 받음 파일크기는 약 35000byte이고 전체를 받으면 빠를것으로 생각됨
           BufferedReader reader = new BufferedReader(
                   new FileReader("C:\\Users\\admin\\Downloads\\"+args[0]),40000
               );
           Path path = Paths.get("C:\\Users\\admin\\Downloads\\"+args[0]);
           //문장의 개수를 미리 알면 속도향상에 도움이 될것으로 생각해서 문장의 갯수 확인
           int lineCount = (int) Files.lines(path).count();
           String[] saetze = new String[lineCount];
           final int [] count = {0};
           //읽은 문장들을 배열에 저장
           Files.lines(path).forEach(line -> saetze[count[0]++] = line);
           reader.close();
           
           long end = System.currentTimeMillis();
           double time = (end - start)/1000.0;
           System.out.println(String.format("Complete! (%.3fs)", time));
           
           //멀티쓰레들 사용하면 기존보다 더 빨라질것으로 생각됨
           //containskey 보다 get함수가 빠르지만 큰 차이 없음
           long start1 = System.currentTimeMillis();

           
           //해쉬맵을 2중으로 사용
           HashMap<String, HashMap<Integer,Integer>> doc = new HashMap<String, HashMap<Integer,Integer>>();
           
           //문장들을 병렬처리
           Iterator<String> iterator = Arrays.stream(saetze).parallel().iterator();
           while (iterator.hasNext()) {
               String satz = iterator.next();
               String[] temp = satz.toLowerCase().replaceAll("[^a-z0-9]", " ").split(" ");
               temp = Arrays.stream(temp).filter(s -> !s.isEmpty()).toArray(String[]::new);
               int doc_id = Integer.parseInt(temp[0]);
               for (int i = 1; i < temp.length; i++) {
                   String word = temp[i];
                   
                	   
                       if(!doc.containsKey(word)) {
                    	   
                           doc.put(word, new HashMap<Integer,Integer>());
                    	   
                       }
                       
                       HashMap<Integer,Integer> innerMap = doc.get(word);
                       innerMap.put(doc_id, innerMap.getOrDefault(doc_id, 0) + 1);
                	   
                   
                  
               }
               
           }
           Comparator<HashMap.Entry<Integer, Integer>> valueComparator = new Comparator<HashMap.Entry<Integer, Integer>>() {
               @Override
               public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
                   int compare = o2.getValue().compareTo(o1.getValue()); // comparing values in descending order
                   if (compare == 0) {
                       return o1.getKey().compareTo(o2.getKey()); // comparing keys in ascending order
                   }
                   return compare;
               }
           };
           
           HashMap<String, LinkedHashMap<Integer,Integer>> sortedDoc = new HashMap<>();
           Set<String> keySet = doc.keySet();
           List<String> keyList = new ArrayList<>(keySet);
           Collections.sort(keyList);
           for (String key : keyList) {
               HashMap<Integer,Integer> innerMap = doc.get(key);
               List<Map.Entry<Integer, Integer>> list = new ArrayList<>(innerMap.entrySet());
               // comparator로 정렬
               Collections.sort(list,valueComparator);
               // comparator로 정렬된 결과를 새로운 해쉬맵에 저장
               LinkedHashMap<Integer,Integer> innerMapSorted = new LinkedHashMap<>();
               for(HashMap.Entry<Integer, Integer> innerEntry : list) {
                   innerMapSorted.put(innerEntry.getKey(), innerEntry.getValue());
               }
               sortedDoc.put(key, innerMapSorted);
           }
           /*
           Set<String> li = sortedDoc.keySet();         
           for (String w : li) {            	   
               LinkedHashMap<Integer, Integer> inn = sortedDoc.get(w);
               Set<Integer> inner = inn.keySet();
               for (Integer docId : inner) {
                   System.out.println(w+" "+ docId + " " + inn.get(docId) + " ");
               }
           }
           */
           
          
           BufferedWriter bw = null;
           try {
               bw = new BufferedWriter(new FileWriter("sortedDoc.txt"),1024);
               List<String> sortedDocKeySet = keyList;    
 
               for (String word : sortedDocKeySet) {            	   
                   LinkedHashMap<Integer, Integer> innerMap = sortedDoc.get(word);
                   Set<Integer> innerMapKeySet = innerMap.keySet();
                   for (Integer docId : innerMapKeySet) {
                       bw.write(word+" "+ docId + " " + innerMap.get(docId) + " ");
                   }
                   bw.newLine();
                   bw.flush();
               }
           } catch (IOException e) {
               e.printStackTrace();
           } finally {
               try {
                   if (bw != null)
                       bw.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }

     
 
                              
           
           long end1 = System.currentTimeMillis();
           double time1 = (end1 - start1)/1000.0;
           System.out.println(String.format("Complete! (%.3fs)", time1));
           System.out.println(String.format("total Complete! (%.3fs)", time1+time));
           //결과를 정렬하여 출력 빈도수에 내림차순 빈도수가 같을때 문서아이디로 오름차순
           /*
           String url;
           if (args[0].equals("input.big")) url = "C:\\Users\\admin\\Downloads\\output.big";
           else url = "C:\\Users\\admin\\Downloads\\output.small";
           
           System.out.println("read output file");
           BufferedReader reader2 = new BufferedReader(
                   new FileReader(url),40000
               );
           Path path2 = Paths.get(url);
           //문장의 개수를 미리 알면 속도향상에 도움이 될것으로 생각해서 문장의 갯수 확인
           int lineCount2 = (int) Files.lines(path2).count();
           String[] saetze2 = new String[lineCount2];
           String str2;
           int count2 = 0;
           //읽은 문장들을 배열에 저장
           while ((str2 = reader2.readLine()) != null) {
               saetze2[count2] = str2;
               count2 = count2 + 1;
           }
           reader2.close();
           System.out.println("test, whether result is correct or not");
           Set<String> set2 = new HashSet<>();
           for (int i = 0; i < lineCount2; i++) {
        	   String[] output = saetze2[i].split(" ");
        	   HashMap <Integer,Integer>eval = sortedDoc.get(output[0]);
        	   set2.add(output[0]);
        	   int cnt = 0;
               for (Map.Entry<Integer, Integer> entry : eval.entrySet()) {
            	   if (entry.getKey() != Integer.parseInt(output[1+cnt*2]) || entry.getValue() != Integer.parseInt(output[2+cnt*2])){
                     System.out.println("the result is false.");          
               }
               cnt++;
               }
           }    
           
           System.out.println("test complete.");
           */
           
           boolean implementing = true;
           int cnt = 0;
           while (implementing) {
              Scanner sc = new Scanner(System.in);
              System.out.printf("input : ");
               String input = sc.next();
               
               
               if (sortedDoc.containsKey(input) == true) {
               HashMap <Integer,Integer>map2 = sortedDoc.get(input);
               for (Map.Entry<Integer, Integer> entry : map2.entrySet()) {
                   System.out.println("Doc: " + entry.getKey() + ", Freuquency: " + entry.getValue());          
               }
              System.out.println(" ");
              cnt++;
               }
               else {
                  System.out.println("The Word doesn't exist");
               }
              if(cnt == 2) {
                  implementing = false;
                  sc.close();
              }
               
           }

}
}
